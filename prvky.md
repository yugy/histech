# PRVKY

## Obsah
* [Vznik elementů v přírod](#k_vznik_priroda)
* [Radioaktivita a poločas rozpadu](#k_radioaktivita)
* [Nová jména](#k_k_nova_jmena)
* [Dočasné názvy](#k_docasne_nazvy)
* [České názvy](#k_ceske_nazvy)
* [Zdroje](#k_zdroje)
	
	
TODO stručně vysvětlit co je atom a co jsou izotopy 

TODO zmínit rozdíl mezi fůzí a štěpením 

## Vznik elementů v přírodě <a name="k_vznik_priroda"></a>


<table>
  <tr>
    <th>Prvek</th>
    <th>Zastoupení ve vesmíru</th>
  </tr>
  <tr>
    <td>Vodík</td>
    <td>75%</td>
  </tr>
  <tr>
    <td>Helium</td>
    <td>23%</td>
  </tr>
  <tr>
    <td>Kyslík</td>
    <td>1%</td>
  </tr>
  <tr>
    <td>Uhlík</td>
    <td>0.5%</td>
  </tr>
  <tr>
    <td>Neon</td>
    <td>0.13%</td>
  </tr>
  <tr>
    <td>Železo</td>
    <td>0.11%</td>
  </tr>
  <tr>
    <td>Dusík</td>
    <td>0.1%</td>
  </tr>
  <tr>
    <td>Křemen</td>
    <td>0.07%</td>
  </tr>
<tr>
    <td>Hořčík</td>
    <td>0.059%</td>
  </tr>
<tr>
    <td>Síra</td>
    <td>0.05%</td>
  </tr>
</table>

## Radioaktivita a poločas rozpadu <a name="k_radioaktivita"></a>

Některá jádra prvků a izotopů nejsou stabilní. Jejich jádra mají špatný počet protonů, nebo neutronů, nebo jsou příliš velká. Postupně dochází k jejich rozpadu a ztrátě energie zářením (latinsky radiací).

Všechny prvky s atomovým číslem 83 nebo vyšším jsou nestabilní. (Některé seznamy neuvádějí 83. prvek bismut jako radioaktivní, protože je tak málo radioaktivní skoro jako kdyby nebyl.)

Je nemožné přesně předpovědět za jak dlouho se atom prvku rozpadne. Pokud je jich ale větší množství, dá se statisticky odhadnout *poločas rozpadu* (anglickly half-life), tedy doba během níž dojde k rozpadu poloviny atomů. Potřebná doba se liší, u některých izotopů jsou to miliardy let, u jiných jenom zlomky sekundy. 

<table>
   <tr>
		<th>Prvek</th>
		<th>Poločas rozpadu</th>
	</tr>
	<tr>
    <th>Oganesson</th>
	<td>5 milisekund</td>
  </tr>
  <tr>
    <td>Tennessine</td>
	<td>50 milisekund</td>
  </tr>
  <tr>
    <td>Livermorium</td>
	<td>120 milisekund</td>
  </tr>
  <tr>
    <td>Moscovium</td>
	<td>1 minuta</td>
  </tr>
  <tr>
    <td>Flerovium</td>
	<td>1.3 minut</td>
  </tr>
  <tr>
    <td>Darmstadtium</td>
    <td>4 m</td>
  </tr>
  <tr>
    <td>Roentgenium</td>
	<td>10 minut</td>
  </tr>
  <tr>
    <td>Nihonium</td>
    <td>20 minut</td>
  </tr>
  <tr>
    <td>Francium</td>
	<td>22 minut</td>
  </tr>
  <tr>
    <td>Meitnerium</td>
	<td>30 minut</td>
  </tr>
  <tr>
    <td>Copernicium</td>
	<td>40 minut</td>
  </tr>
  <tr>
    <td>Hassium</td>
	<td>1.1 hodina</td>
  </tr>
  <tr>
    <td>Bohrium</td>
	<td>1.5 hodin</td>
  </tr>
  <tr>
    <td>Seaborgium</td>
	<td>1.9 hodin</td>
  </tr>
  <tr>
    <td>Nobelium</td>
	<td>2.8 hodin</td>
  </tr>
  <tr>
    <td>Dubnium</td>
	<td>5.6 hodin</td>
  </tr>
  <tr>
    <td>Astatine</td>
	<td>8 hodin</td>
  </tr>
  <tr>
    <td>Lawrencium</td>
	<td>10 hodin</td>
  </tr>
  <tr>
    <td>Rutherfordium</td>
	<td>13 hodin</td>
  </tr>
  <tr>
    <td>Radon</td>
	<td>3.8 dní</td>
  </tr>
  <tr>
    <td>Mendelevium</td>
	<td>52 dní</td>
  </tr>
  <tr>
    <td>Fermium</td>
	<td>100 dní</td>
  </tr>
  <tr>
    <td>Einsteinium</td>
	<td>1.3 let</td>
  </tr>
  <tr>
    <td>Promethium</td>
	<td>18 let</td>
  </tr>
  <tr>
    <td>Actinium</td>
	<td>22 let</td>
  </tr>
  <tr>
    <td>Polonium</td>
	<td>102 let</td>
  </tr>
  <tr>
    <td>Californium</td>
	<td>901 let</td>
  </tr>
  <tr>
    <td>Berkelium</td>
	<td>1 379 let</td>
  </tr>
  <tr>
    <td>Radium</td>
	<td>1 585 let</td>
  </tr>
  <tr>
    <td>Americium</td>
	<td>7 388 let</td>
  </tr>
  <tr>
    <td>Protactinium</td>
	<td>32 788 let</td>
  </tr>
  <tr>
    <td>Technetium</td>
	<td>211 098 let</td>
  </tr>
  <tr>
    <td>Neptunium</td>
	<td>2 miliony let</td>
  </tr>
  <tr>
    <td>Curium</td>
	<td>16 milionu let</td>
  </tr>
  <tr>
    <td>Plutonium</td>
	<td>79 milionu let</td>
  </tr>
  <tr>
    <td>Uranium</td>
	<td>4.5 miliardy let</td>
  </tr>
  <tr>
    <td>Thorium</td>
    <td>14 miliard let</td>
  </tr>
  <tr>
    <td>Bismuth</td>
	<td>19 trilionů let</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
</table>


Pokud víme, že planeta Země je stará 4.54 miliardy let, je očividně proč se většína radioaktivních prvků v přírodě normálně nevyskytuje. 

Pouze bismut, thorium a uran zůstali od stvoření Země nezměněny, ostatní radioaktivní prvky přirozeně nacházející se v současnosti na naší planetě jsou produktem jejich rozpadu. Zde je například zobrazena rozpadová řada uranu-238, který se postupně rozpadá na throrium-234, to na proactinium-234 a tak dále až ke stabilnímu olovu-206:

<table>
  <tr>
    <th>Izotop</th>
    <th>vydává záření</th>
    <th>a v poločase se rozpadne na</th>
  </tr>
  <tr>
    <td>Uran-238</td>
    <td>alfa</td>
    <td>4.5 miliardy let</td>
  </tr>
  <tr>
    <td>Thorium</td>
    <td>beta</td>
    <td>24 dní</td>
  </tr>
  <tr>
    <td>Proactinium</td>
    <td>beta</td>
    <td>1.2 minut</td>
  </tr>
  <tr>
    <td>Uran-234</td>
    <td>alfa</td>
    <td>240 tisíc let</td>
  </tr>
  <tr>
    <td>Thorium-230</td>
    <td>alfa</td>
    <td>77 tisíc let</td>
  </tr>
  <tr>
    <td>Radium-226</td>
    <td>alfa</td>
    <td>1.6 tisíc let</td>
  </tr>
  <tr>
    <td>Radon-222</td>
    <td>alfa</td>
    <td>3.8 dní</td>
  </tr>
  <tr>
    <td>Polonium-218</td>
    <td>alfa</td>
    <td>3.1 minut</td>
  </tr>
<tr>
    <td>Olovo-214</td>
    <td>beta</td>
    <td>27 minut</td>
  </tr>
<tr>
    <td>Bismut-214</td>
    <td>beta</td>
    <td>20 minut</td>
  </tr>
<tr>
    <td>Polonium-214</td>
    <td>alfa</td>
    <td>160 mikrosekund</td>
  </tr>
<tr>
    <td>Olovo-210</td>
    <td>beta</td>
    <td>22 let</td>
  </tr>
<tr>
    <td>Bismut-210</td>
    <td>beta</td>
    <td>5 dní</td>
  </tr>
<tr>
    <td>Polonium-210</td>
    <td>alfa</td>
    <td>140 dní</td>
  </tr>
<tr>
    <td>Olovo-206</td>
    <td></td>
    <td></td>
  </tr>
</table>


## Nová jména <a name="k_nova_jmena"></a>

Jména některých prvků jsou známa odedávna. Jiné byly pojmenovány teprve nedávno. Nová jména jsou obecně inspirováná vlastnotmi prvků, místem objevu, mytologickými postavami, astronomickýmo objekty nebo jmény předních vědců. 

Aby se předešlo sporům , bylo v roce 1947 rozhodnuto, že objevitel má právo pouze *navrhout* jméno prvku, ale konečné rohodnutí je na Mezinárodní unii pro čistou a užitou chemii (IUPAC, *International Union of Pure and Applied Chemistry*). 

Například niob byl objeven anglickým chemikem Charlesem Hatchettem roku 1801. Nalezl ho v nerostu z Ameriky a proto ho pojmenoval *columbium*. Jeho objev se ale nepodařilo potvrdit. Prvek by znovu objeven roku 1846 němcem Heinrichem Rose, který ho nazval *niobium* (po Niobe, dceři krále Tantala, po němž je pojmenovaný tantal, který má podobné chemické vlastnosti). Zde vyvstal problém, protože zatímco Evropané používali označení niobium, columbium se ujalo v Americe. Nakonec IUPAC vše vyřešila kompromisem - officiálním názvem se stalo *niobium* ale officialním názvem wolframu se stal v Americe preferovaný *tungsten* namísto evropského *wolfram*.




## Dočasné názvy <a name="k_docasne_nazvy"></a>

Dočasné názvy ještě nepojmenovaných nebo neobjevených prvků jsou odvozeny z protonového čísla, tedy z počtu protonů v jejich jádře. Jednotlivé číslovky jsou nahrazeny kořeny latinských nebo řeckých čísel. Jsou spojeny dohromady a nakonec je přidána koncovka -ium

<table>
	 <tr>
		<th>Číslice</th>
		<th>Kořen</th>
		<th>Původ</th>
		<th>Symbol</th>
	</tr>
	<tr>
		<td>0</td>
		<td>nil</td>
		<td>latinské *nihil* - nic</td>
		<td>n</td>
	</tr>
	<tr>
		<td>1</td>
		<td>un</td>
		<td>latinské *unus* - jeden</td>
		<td>u</td>
	</tr>
	<tr>
		<td>2</td>
		<td>b(i)</td>
		<td>latinské *bis* - dvakrát </td>
		<td>b</td>
	</tr>
	<tr>
		<td>3</td>
		<td>tr(i)</td>
		<td>latinské *tres* - tři; řecké *tria* - tři</td>
		<td>t</td>
	</tr>
	<tr>
		<td>4</td>
		<td>quad</td>
		<td>latinské *quattuor* - čtyři</td>
		<td>q</td>
	</tr>
	<tr>
		<td>5</td>
		<td>pent</td>
		<td>řecké *pente* - pět</td>
		<td>p</td>
	</tr>
	<tr>
		<td>6</td>
		<td>hex</td>
		<td>řecké *hex* - šest</td>
		<td>h</td>
	</tr>
	<tr>
		<td>7</td>
		<td>sept</td>
		<td>latinské *septem* - sedm </td>
		<td>s</td>
	</tr>
	<tr>
		<td>8</td>
		<td>oct</td>
		<td>latinské *octo* - osm; řecké *okto* - osm</td>
		<td>o</td>
	</tr>
	<tr>
		<td>9</td>
		<td>en(n)</td>
		<td>řecké *ennea* - devět</td>
		<td>e</td>
	</tr>
	<tr>
		<td>Suffix</td>
		<td>-ium</td>
		<td>latinské *-um* - koncovka jednotného čísla středního rodu</td>
		<td></td>
	</tr>
</table>

Tedy například prvek 117 (Tennessin) byl dočasně znám jako un - un - sept - ium, *ununseptium* a měl symbol Uus. Dosud neobjevený prvek 119 nese dočasné jméno *Ununennium*. 

## České názvy <a name="k_ceske_nazvy"></a>

Snahám obrozenců neunikly ani chemické prvky. Jan Svatopluk Presl (1791 - 1849), jeden z nejvýznačnějších českých přírodovědců 19. století je zodpovědný nejenom za české pojmenování zvířat jako lachtan, vorvaň, hrocha nebo klokan (jako skokan ale s K) ale i za české pojmenování chemických prvků. Deset z nich se ujalo a jsou používány dodnes: kyslík, dusík, uhlík, vodík, hliník, hořčík, křemík, draslík, sodík, vápník a nikomu nepřijdou divné. 

Ostatní byly zapomenuty což je možná škoda. Fosfor, který se nachází v kostech byl pojmenován *kostík*, chlor obsažený v kuchyňské soli *solík*, jód, který je k nalezenív chaluhách *chaluzík*. Kobaltu, pojmenovaném po německých důlních skřetech koboldech říkal *ďasík*.    Řecké názvy prvků chrom, osmium a selen byly přeloženy do Češtiny jako *barvík*, *voník* a *luník*.




## Zdroje <a name="k_zdroje"></a>



- Koppenol, Willem H., John Corish, Javier García-Martínez, Juris Meija, and Jan Reedijk. “[How to Name New Chemical Elements (IUPAC Recommendations 2016)](http://doi.org/10.1515/pac-2015-0802).” Pure and Applied Chemistry 88, no. 4 (January 2016): 401–5.
- Murphy, Edward. "[The Origin of the Elements](https://www.youtube.com/watch?v=ZJQjjBR6PbY). " Jefferson Lab. youtube.com.
- Novotný, Michal."[Ďasík, voník, ytřík a platík](https://region.rozhlas.cz/dasik-vonik-ytrik-a-platik-7290944)." region.rozhlas.cz, 1. duben, 2015.

- "[Hmotozpyt čili lučba](http://canov.jergym.cz/objevite/objevite/tabulka_2.html)." canov.jergym.cz, 
- "[Half Life of the elements](https://periodictable.com/Properties/A/HalfLife.html)." periodictable.com. 
- "[Abundance in the Universe of the elements](https://periodictable.com/Properties/A/UniverseAbundance.log.html)." periodictable.com.
- "[Systematic element name](https://en.wikipedia.org/wiki/Systematic_element_name)." Wikipedia, changed 23 September, 2019.
- "[Radioactive decay](https://en.wikipedia.org/wiki/Radioactive_decay)." Wikipedia, changed 2 February, 2020.
- "[List of chemical element name etymologies](https://en.wikipedia.org/wiki/List_of_chemical_element_name_etymologies)." Wikipedia, changed 7 December, 2019.


<!---

## Tabulka

<table id="tg-KIrGy">
	<tr>
		<th>Protonové číslo</th>
		<th>Název</th>
		<th>Rok objevu</th>
		<th>Etymologie</th>
		<th>Český název</th>
		<th>Etymologie</th>
	</tr>
	<tr>
		<td>15</td>
		<td>Phosphorus</td>
		<td>1669</td>
		<td>phos + phoros</td>
		<td>světlonoš</td>
		<td>Fosfor</td>
		<td></td>
	</tr>
	<tr>
		<td>27</td>
		<td>Cobalt</td>
		<td>1735</td>
		<td>Kobold</td>
		<td>důlní skřet</td>
		<td>Kobalt</td>
		<td></td>
	</tr>
	<tr>
		<td>78</td>
		<td>Platinum</td>
		<td>1735</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>28</td>
		<td>Nickel</td>
		<td>1751</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>12</td>
		<td>Magnesium</td>
		<td>1755</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1</td>
		<td>Hydrogen</td>
		<td>1766</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>8</td>
		<td>Oxygen</td>
		<td>1771</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>7</td>
		<td>Nitrogen</td>
		<td>1772</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>56</td>
		<td>Barium</td>
		<td>1772</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>17</td>
		<td>Chlorine</td>
		<td>1774</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>25</td>
		<td>Manganese</td>
		<td>1774</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>42</td>
		<td>Molybdenum</td>
		<td>1778</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>74</td>
		<td>Tungsten</td>
		<td>1781</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>52</td>
		<td>Tellurium</td>
		<td>1782</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>38</td>
		<td>Strontium</td>
		<td>1787</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>1789</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>40</td>
		<td>Zirconium</td>
		<td>1789</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>92</td>
		<td>Uranium</td>
		<td>1789</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>22</td>
		<td>Titanium</td>
		<td>1791</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>39</td>
		<td>Yttrium</td>
		<td>1794</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>24</td>
		<td>Chromium</td>
		<td>1794</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>4</td>
		<td>Beryllium</td>
		<td>1798</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>23</td>
		<td>Vanadium</td>
		<td>1801</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>41</td>
		<td>Niobium</td>
		<td>1801</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>73</td>
		<td>Tantalum</td>
		<td>1802</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>46</td>
		<td>Palladium</td>
		<td>1802</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>58</td>
		<td>Cerium</td>
		<td>1803</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>76</td>
		<td>Osmium</td>
		<td>1803</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>77</td>
		<td>Iridium</td>
		<td>1803</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>45</td>
		<td>Rhodium</td>
		<td>1804</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>19</td>
		<td>Potassium</td>
		<td>1807</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>11</td>
		<td>Sodium</td>
		<td>1807</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>20</td>
		<td>Calcium</td>
		<td>1808</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>5</td>
		<td>Boron</td>
		<td>1808</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>9</td>
		<td>Fluorine</td>
		<td>1810</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>53</td>
		<td>Iodine</td>
		<td>1811</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>3</td>
		<td>Lithium</td>
		<td>1817</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>48</td>
		<td>Cadmium</td>
		<td>1817</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>34</td>
		<td>Selenium</td>
		<td>1817</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>14</td>
		<td>Silicon</td>
		<td>1823</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>13</td>
		<td>Aluminium</td>
		<td>1825</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>35</td>
		<td>Bromine</td>
		<td>1825</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>90</td>
		<td>Thorium</td>
		<td>1829</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>57</td>
		<td>Lanthanum</td>
		<td>1838</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>68</td>
		<td>Erbium</td>
		<td>1843</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>65</td>
		<td>Terbium</td>
		<td>1843</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>44</td>
		<td>Ruthenium</td>
		<td>1844</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>55</td>
		<td>Caesium</td>
		<td>1860</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>37</td>
		<td>Rubidium</td>
		<td>1861</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>81</td>
		<td>Thallium</td>
		<td>1861</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>49</td>
		<td>Indium</td>
		<td>1863</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>2</td>
		<td>Helium</td>
		<td>1868</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>1869</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>31</td>
		<td>Gallium</td>
		<td>1875</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>70</td>
		<td>Ytterbium</td>
		<td>1878</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>67</td>
		<td>Holmium</td>
		<td>1878</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>69</td>
		<td>Thulium</td>
		<td>1879</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>21</td>
		<td>Scandium</td>
		<td>1879</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>62</td>
		<td>Samarium</td>
		<td>1879</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>64</td>
		<td>Gadolinium</td>
		<td>1880</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>59</td>
		<td>Praseodymium</td>
		<td>1885</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>60</td>
		<td>Neodymium</td>
		<td>1885</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>32</td>
		<td>Germanium</td>
		<td>1886</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>66</td>
		<td>Dysprosium</td>
		<td>1886</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>18</td>
		<td>Argon</td>
		<td>1894</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>63</td>
		<td>Europium</td>
		<td>1896</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>36</td>
		<td>Krypton</td>
		<td>1898</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>10</td>
		<td>Neon</td>
		<td>1898</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>54</td>
		<td>Xenon</td>
		<td>1898</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>84</td>
		<td>Polonium</td>
		<td>1898</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>88</td>
		<td>Radium</td>
		<td>1898</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>86</td>
		<td>Radon</td>
		<td>1899</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>89</td>
		<td>Actinium</td>
		<td>1902</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>71</td>
		<td>Lutetium</td>
		<td>1906</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>75</td>
		<td>Rhenium</td>
		<td>1908</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>91</td>
		<td>Protactinium</td>
		<td>1913</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>72</td>
		<td>Hafnium</td>
		<td>1922</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>43</td>
		<td>Technetium</td>
		<td>1937</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>87</td>
		<td>Francium</td>
		<td>1939</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>93</td>
		<td>Neptunium</td>
		<td>1940</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>85</td>
		<td>Astatine</td>
		<td>1940</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>94</td>
		<td>Plutonium</td>
		<td>1940–1941</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>61</td>
		<td>Promethium</td>
		<td>1942</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>96</td>
		<td>Curium</td>
		<td>1944</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>95</td>
		<td>Americium</td>
		<td>1944</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>97</td>
		<td>Berkelium</td>
		<td>1949</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>98</td>
		<td>Californium</td>
		<td>1950</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>99</td>
		<td>Einsteinium</td>
		<td>1952</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>100</td>
		<td>Fermium</td>
		<td>1952</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>101</td>
		<td>Mendelevium</td>
		<td>1955</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>103</td>
		<td>Lawrencium</td>
		<td>1961</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>102</td>
		<td>Nobelium</td>
		<td>1966</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>104</td>
		<td>Rutherfordium</td>
		<td>1969</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>105</td>
		<td>Dubnium</td>
		<td>1970</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>106</td>
		<td>Seaborgium</td>
		<td>1974</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>107</td>
		<td>Bohrium</td>
		<td>1981</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>109</td>
		<td>Meitnerium</td>
		<td>1982</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>108</td>
		<td>Hassium</td>
		<td>1984</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>110</td>
		<td>Darmstadtium</td>
		<td>1994</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>111</td>
		<td>Roentgenium</td>
		<td>1994</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>112</td>
		<td>Copernicium</td>
		<td>1996</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>114</td>
		<td>Flerovium</td>
		<td>1999</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>116</td>
		<td>Livermorium</td>
		<td>2000</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>118</td>
		<td>Oganesson</td>
		<td>2002</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>115</td>
		<td>Moscovium</td>
		<td>2003</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>113</td>
		<td>Nihonium</td>
		<td>2003–2004</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>117</td>
		<td>Tennessine</td>
		<td>2009</td>
		<td></td>
		<td></td>
	<td></td>
	</tr>

</table>
<script charset="utf-8">var TGSort=window.TGSort||function(n){"use strict";function r(n){return n.length}function t(n,t){if(n)for(var e=0,a=r(n);a>e;++e)t(n[e],e)}function e(n){return n.split("").reverse().join("")}function a(n){var e=n[0];return t(n,function(n){for(;!n.startsWith(e);)e=e.substring(0,r(e)-1)}),r(e)}function o(n,r){return-1!=n.map(r).indexOf(!0)}function u(n,r){return function(t){var e="";return t.replace(n,function(n,t,a){return e=t.replace(r,"")+"."+(a||"").substring(1)}),l(e)}}function i(n){var t=l(n);return!isNaN(t)&&r(""+t)+1>=r(n)?t:NaN}function s(n){var e=[];return t([i,m,g],function(t){var a;r(e)||o(a=n.map(t),isNaN)||(e=a)}),e}function c(n){var t=s(n);if(!r(t)){var o=a(n),u=a(n.map(e)),i=n.map(function(n){return n.substring(o,r(n)-u)});t=s(i)}return t}function f(n){var r=n.map(Date.parse);return o(r,isNaN)?[]:r}function v(n,r){r(n),t(n.childNodes,function(n){v(n,r)})}function d(n){var r,t=[],e=[];return v(n,function(n){var a=n.nodeName;"TR"==a?(r=[],t.push(r),e.push(n)):("TD"==a||"TH"==a)&&r.push(n)}),[t,e]}function p(n){if("TABLE"==n.nodeName){for(var e=d(n),a=e[0],o=e[1],u=r(a),i=u>1&&r(a[0])<r(a[1])?1:0,s=i+1,v=a[i],p=r(v),l=[],m=[],g=[],h=s;u>h;++h){for(var N=0;p>N;++N){r(m)<p&&m.push([]);var T=a[h][N],C=T.textContent||T.innerText||"";m[N].push(C.trim())}g.push(h-s)}var L="tg-sort-asc",E="tg-sort-desc",b=function(){for(var n=0;p>n;++n){var r=v[n].classList;r.remove(L),r.remove(E),l[n]=0}};t(v,function(n,t){l[t]=0;var e=n.classList;e.add("tg-sort-header"),n.addEventListener("click",function(){function n(n,r){var t=d[n],e=d[r];return t>e?a:e>t?-a:a*(n-r)}var a=l[t];b(),a=1==a?-1:+!a,a&&e.add(a>0?L:E),l[t]=a;var i=m[t],v=function(n,r){return a*i[n].localeCompare(i[r])||a*(n-r)},d=c(i);(r(d)||r(d=f(i)))&&(v=n);var p=g.slice();p.sort(v);for(var h=null,N=s;u>N;++N)h=o[N].parentNode,h.removeChild(o[N]);for(var N=s;u>N;++N)h.appendChild(o[s+p[N-s]])})})}}var l=parseFloat,m=u(/^(?:\s*)([+-]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g,/,/g),g=u(/^(?:\s*)([+-]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g,/\./g);n.addEventListener("DOMContentLoaded",function(){for(var t=n.getElementsByClassName("tg"),e=0;e<r(t);++e)try{p(t[e])}catch(a){}})}(document);</script>



<table>
   <tr>
		<th>Prvek</th>
		<th>Poločas rozpadu</th>
	</tr>
	<tr>
    <th>Oganesson</th>
    <th>5 ms</th>
	<td>5 milisekund</td>
  </tr>
  <tr>
    <td>Tennessine</td>
    <td>50 ms</td>
	<td>50 milisekund</td>
  </tr>
  <tr>
    <td>Livermorium</td>
    <td>120 ms</td>
	<td>120 milisekund</td>
  </tr>
  <tr>
    <td>Moscovium</td>
    <td>1 m</td>
	<td>1 minuta</td>
  </tr>
  <tr>
    <td>Flerovium</td>
    <td>1.316666666667 m</td>
	<td>1.3 minut</td>
  </tr>
  <tr>
    <td>Darmstadtium</td>
    <td>4 m</td>
  </tr>
  <tr>
    <td>Roentgenium</td>
    <td>10 m</td>
	<td>10 minut</td>
  </tr>
  <tr>
    <td>Nihonium</td>
    <td>20 m</td>
	<td>20 minut</td>
  </tr>
  <tr>
    <td>Francium</td>
    <td>21.66666666667 m</td>
	<td>22 minut</td>
  </tr>
  <tr>
    <td>Meitnerium</td>
    <td>30 m</td>
	<td>30 minut</td>
  </tr>
  <tr>
    <td>Copernicium</td>
    <td>40 m</td>
	<td>40 minut</td>
  </tr>
  <tr>
    <td>Hassium</td>
    <td>1.111111111111 h</td>
	<td>1.1 hodina</td>
  </tr>
  <tr>
    <td>Bohrium</td>
    <td>1.5 h</td>
	<td>1.5 hodin</td>
  </tr>
  <tr>
    <td>Seaborgium</td>
    <td>1.944444444444 h</td>
	<td>1.9 hodin</td>
  </tr>
  <tr>
    <td>Nobelium</td>
    <td>2.777777777778 h</td>
	<td>2.8 hodin</td>
  </tr>
  <tr>
    <td>Dubnium</td>
    <td>5.555555555556 h</td>
	<td>5.6 hodin</td>
  </tr>
  <tr>
    <td>Astatine</td>
    <td>8.055555555556 h</td>
	<td>8 hodin</td>
  </tr>
  <tr>
    <td>Lawrencium</td>
    <td>10 h</td>
	<td>10 hodin</td>
  </tr>
  <tr>
    <td>Rutherfordium</td>
    <td>13.05555555556 h</td>
	<td>13 hodin</td>
  </tr>
  <tr>
    <td>Radon</td>
    <td>3.82349537037 d</td>
	<td>3.8 dní</td>
  </tr>
  <tr>
    <td>Mendelevium</td>
    <td>51.50462962963 d</td>
	<td>52 dní</td>
  </tr>
  <tr>
    <td>Fermium</td>
    <td>100.4976851852 d</td>
	<td>100 dní</td>
  </tr>
  <tr>
    <td>Einsteinium</td>
    <td>1.292174023338 y</td>
	<td>1.3 let</td>
  </tr>
  <tr>
    <td>Promethium</td>
    <td>17.72577371892 y</td>
	<td>18 let</td>
  </tr>
  <tr>
    <td>Actinium</td>
    <td>21.78652968037 y</td>
	<td>22 let</td>
  </tr>
  <tr>
    <td>Polonium</td>
    <td>102.1055301877 y</td>
	<td>102 let</td>
  </tr>
  <tr>
    <td>Californium</td>
    <td>900.5580923389 y</td>
	<td>901 let</td>
  </tr>
  <tr>
    <td>Berkelium</td>
    <td>1379.375951294 y</td>
	<td>1 379 let</td>
  </tr>
  <tr>
    <td>Radium</td>
    <td>1585.489599188 y</td>
	<td>1 585 let</td>
  </tr>
  <tr>
    <td>Americium</td>
    <td>7388.381532217 y</td>
	<td>7 388 let</td>
  </tr>
  <tr>
    <td>Protactinium</td>
    <td>32787.92491121 y</td>
	<td>32 788 let</td>
  </tr>
  <tr>
    <td>Technetium</td>
    <td>211098.4271943 y</td>
	<td>211 098 let</td>
  </tr>
  <tr>
    <td>Neptunium</td>
    <td>2.145484525622×106 y</td>
	<td>2 miliony let</td>
  </tr>
  <tr>
    <td>Curium</td>
    <td>1.560121765601×107 y</td>
	<td>16 milionu let</td>
  </tr>
  <tr>
    <td>Plutonium</td>
    <td>7.927447995941×107 y</td>
	<td>79 milionu let</td>
  </tr>
  <tr>
    <td>Uranium</td>
    <td>4.471080669711×109 y</td>
	<td>4.5 miliardy let</td>
  </tr>
  <tr>
    <td>Thorium</td>
    <td>14 miliard let</td>
  </tr>
  <tr>
    <td>Bismuth</td>
    <td>1.902587519026×1019 y</td>
	<td>19 trilionů let</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
</table>


-->